#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

import re
import hashlib

weights = [
    ['5', 75],   ['6', 75],   ['7', 75],   ['8', 75],   ['16', 75],
    ['17', 75],  ['18', 75],  ['9', 95],   ['11', 95],  ['12', 95],
    ['13', 95],  ['14', 95],  ['15', 95],  ['19', 110], ['23', 110],
    ['24', 110], ['25', 110], ['26', 110], ['28', 104], ['29', 104],
    ['30', 104], ['31', 104], ['32', 104], ['33', 104], ['35', 101],
    ['36', 101], ['37', 101], ['38', 101], ['39', 101], ['40', 101],
    ['41', 101], ['42', 101], ['43', 101], ['44', 101], ['45', 101],
    ['46', 101], ['47', 101], ['48', 101], ['49', 101], ['50', 101],
    ['52', 110], ['53', 110], ['55', 110], ['57', 110], ['58', 110],
    ['59', 110], ['60', 110], ['61', 110], ['62', 110], ['63', 110],
    ['64', 110], ['65', 110], ['66', 110], ['68', 95],  ['71', 116],
    ['72', 116], ['73', 116], ['74', 116], ['75', 116], ['76', 116],
    ['77', 116], ['78', 116], ['79', 116], ['80', 116], ['81', 116],
    ['82', 116], ['83', 116], ['84', 116]
]

specials = {
    "b55279b3166dd5d30767d68b50c333ab": 21, # kiiiikiii
    "0a249c2a3a3dcb7e40dcfac36bec194f": 21, # cricket365live
    "3ae9453fa1557dc71316701777c5ee42": 51, # cricvid-hitcric-
    "ebcd66fd5b868f249c1952af02a91cb3": 5,  # de-livechat
    "4913527f3dd834ec1bb3e1eb886b6d62": 56, # sport24lt
    "7a067398784395c6208091bc6c3f3aac": 22, # watchanimeonn
    "ce7b7bc84a4e8184edb432085883ae04": 51, # 
    "fe8d11abb9c391d5f2494d48bb89221b": 8,  # watch-dragonball
    "2d14c18e510a550f0d13eac7685ba496": 8,  # ver-anime
    "3e772eba0dfbf48d47b4c02d5a3beac9": 56, # mitvcanal
    "eff4fd30f3fa53a4a1cb3442a951ad03": 54, # 
    "082baeccd5eabe581cba35bd777b93ef": 56, # eafangames
    "e21569f6966d79cfc1b911681182f71f": 34, # animeultimacom
    "0b18ed3fb935c9607cb01cc537ec854a": 10, # narutowire
    "20e46ddc5e273713109edf7623d89e7a": 22, # pokemonepisodeorg
    "72432e25656d6b7dab98148fbd411435": 70, # narutochatt
    "bb02562ba45ca77e62538e6c5db7c8ae": 10, # dbzepisodeorg
    "d78524504941b97ec555ef43c4fd9d3c": 21, # vipstand
    "2db735f3815eec18b4326bed35337441": 56, # stream2watch3
    "63ff05c1d26064b8fe609e40d6693126": 56, # ttvsports
    "ec580e6cbdc2977e09e01eb6a6c62218": 69, # peliculas-flv
    "246894b6a72e704e8e88afc67e8c7ea9": 20, # animelinkz
    "028a31683e35c51862adedc316f9d07b": 51, # rgsmotrisport
    "2b2e3e5ff1550560502ddd282c025996": 27, # leeplarp
    "e0d3ff2ad4d2bedc7603159cb79501d7": 67, # myfoxdfw
    "726a56c70721704493191f8b93fe94a3": 21  # soccerjumbo
}

def get_id(name):
    # NOTE: this is ported from the testj.dev2 code
    name  = strip_url(name)
    hash_ = hashlib.md5(name.encode()).hexdigest()

    if hash_ in specials.keys():
        # this part is obvious
        #print(f"{name} is in specials as {hash} with server {specials[hash]}")
        return str(specials[hash_])

    # reduce charset to base 36
    name = name.replace("_", "q").replace("-", "q")

    # convert the first 5 base 36 characters into a decimal number
    len_ = min(5, len(name))
    gnum = int(name[0:len_], 36)

    # Use up to 3 remaining base 36 digits from the group name to get the modulo
    # Else if not available, use 1000 as the modulus.
    # The modulus should be greater than 1000: small mods = lopsided results
    modStr = name[6:6 + min(3, len(name) - 5)]
    mod = int(modStr, 36) if modStr else 0
    # XXX: the following is a hack due to JS not translating directly to Python
    mod = 1000 if mod < 1000 else mod

    # reduce the large gnum to a range between 0 and 1
    position = (gnum % mod) / mod

    # get total of all weights
    total = sum(map(lambda x: x[1], weights))
#    total = 0
#    for i in range(len(weights)):
#        total += weights[i][1]
    
    # set normalized object
    base = 0
    norm = {}
    for i in range(len(weights)):
        base += weights[i][1] / total
        norm[weights[i][0]] = base
    
    # find the server
    for i in range(len(weights)):
        if position <= norm[weights[i][0]]:
            s_num = weights[i][0]
            break
    
    return s_num # e.g. "5"

def get_host(name):
    name = strip_url(name)
    num  = get_id(name)
    return f"s{num}.chatango.com"

def strip_url(group):
    urlRe = re.compile(r"^http(?:s)?:\/\/(.*?).chatango.com(?:\/)?(?:.+)?$",
                       re.M)
    if urlRe.match(group):
        return urlRe.match(group).group(1)
    else:
        return group