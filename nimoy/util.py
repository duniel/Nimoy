#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

import re
from random import (randrange, random)
from string import (digits, ascii_lowercase)
from textwrap import wrap

# NOTE: don't you fucking judge me.
_N_RE   = re.compile(r"^<n([a-fA-F0-9]+) ?/>")
_F_RE   = re.compile(r'<f x(?P<size>\d{2})?(?P<color>\w{3,6})?="(?P<face>.*?)?">')
_XML_RE = re.compile(r"</?(n|b|i|u|f).*?>")

def parse_msg(raw):
    n_match = _N_RE.search(raw)
    f_match = _F_RE.search(raw)
    msg     = _XML_RE.sub("", raw)
    substitution_map = dict(enumerate(("Arial Comic Georgia Handwriting "
                           "Impact Palatino Papyrus Times Typewriter").split()))

    fface = "Arial"
    fsize = 11
    fcolor = "0"
    ncolor = "0"

    if f_match:
        # NOTE: fix font face
        fface = f_match.group("face") or "Arial"
        if type(fface) == str and fface.isdigit() and int(fface, 10) <= 8:
            fface = substitution_map[int(fface, 10)]

    if n_match:
        ncolor = n_match.group(1)

    if f_match:
        fsize  = f_match.group("size") or 11
        fcolor = f_match.group("color") or "0"

    return {
        "name_color": compress_hex(ncolor),
        "font_face":  fface,
        "font_size":  fsize,
        "font_color": compress_hex(fcolor),
        "body": msg
    }

def expand_hex(hex_):
    if len(hex_) == 1:
        # NOTE: 0 -> 000000
        return hex_ * 6
    elif len(hex_) == 3:
        # NOTE: 0fa -> 00ffaa
        return hex_[0] * 2 + hex_[1] * 2 + hex_[2] * 2
    else:
        return hex_

def compress_hex(hex_):
    if len(hex_) == 1:
        return hex_
    elif len(hex_) == 3:
        # NOTE: bbb -> b
        # NOTE: return `hex_[0]` if r==g==b else `hex_`.
        if len(set(hex_)) == 1:
            return hex_[0]
    elif len(hex_) == 6:
        if len(set(hex_)) == 1:
            # f -> ffffff
            return hex_[0]
        elif all(x[0] == x[1] for x in wrap(hex_, 2)):
            # 00ffaa -> 0fa
            return hex_[0] + hex_[2] + hex_[4]

    return hex_

def get_user_path(user):
    user = user.lower()

    if len(user) == 1:
        return f"{user}/{user}/{user}"
    else:
        # NOTE: "d/y/dynamic"
        return f"{user[0]}/{user[1]}/{user}"

def generate_session_id():
    return randrange(10**15, 10**16 - 1)

def base36encode(number, alphabet=digits + ascii_lowercase):
    # NOTE: https://stackoverflow.com/questions/1181919

    base36 = ""
    sign = ""

    if number < 0:
        sign = "-"
        number = -number

    if 0 <= number < len(alphabet):
        return sign + alphabet[number]

    while number != 0:
        number, i = divmod(number, len(alphabet))
        base36 = alphabet[i] + base36

    return sign + base36

def generate_client_id():
    # NOTE: this can generate 3-character "client IDs". blame chatango.
    return base36encode(round(random() * 1500000))

def get_anon_id(tsid, aid):
    tsid, aid = str(tsid), str(aid)
    # NOTE: the following is the docstring from Chatango

    # A static function for constructing Anon Names
    # @param {string} tsid Timestamp ID, retrieved from the login.Session
    # instance of the current user or from the <n0000/> from an anon's message
    # @param {string} aid The encoded cookie from a message - or the session id
    # @return {string} the anon number

    # tl;dr:
    # tsid = <nXXXX/> or the 4 last digits of str(time.time()).split(".")[0]
    # aid = first 8 numbers of the bauth session id OR the msg shortened_cookie

    if not aid or not tsid:
        return False

    num = aid[4:8]
    out = ""
    for i in range(len(num)):
        n1 = int(num[i:i+1])
        n2 = int(tsid[i:i+1])
        sum_ = str(n1 + n2)
        out = out + sum_[-1]
    
    return out