#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

# NOTE: no, this isn't a CTF

from typing import List, Union
from .error import InvalidFlag

class Flags(object):
    # class for namespacing reasons
    group = {
        "LIST_TAXONOMY": 1, "NOANONS": 4, "NOFLAGGING": 8, "NOCOUNTER": 16,
        "NOIMAGES": 32, "NOLINKS": 64, "NOVIDEOS": 128, "NOSTYLEDTEXT": 256,
        "NOLINKSCHATANGO": 512, "NOBRDCASTMSGWITHBW": 1024,
        "RATELIMITREGIMEON": 2048, "CHANNELSDISABLED":8192,
        "NLP_SINGLEMSG": 16384, "NLP_MSGQUEUE": 32768, "BROADCAST_MODE": 65536,
        "CLOSED_IF_NO_MODS": 131072, "IS_CLOSED": 262144,
        "SHOW_MOD_ICONS": 524288, "MODS_CHOOSE_VISIBILITY": 1048576,
        "NLP_NGRAM": 2097152, "NO_PROXIES": 4194304, "HAS_XML": 268435456,
        "UNSAFE": 536870912
    }

    mod = {
        "DELETED": 1, "EDIT_MODS": 2, "EDIT_MOD_VISIBILITY": 4, "EDIT_BW": 8,
        "EDIT_RESTRICTIONS": 16, "EDIT_GROUP": 32, "SEE_COUNTER": 64,
        "SEE_MOD_CHANNEL": 128, "SEE_MOD_ACTIONS": 256, "EDIT_NLP": 512,
        "EDIT_GP_ANNC": 1024, "EDIT_ADMINS": 2048, "EDIT_SUPERMODS": 4096,
        "NO_SENDING_LIMITATIONS": 8192, "SEE_IPS": 16384, "CLOSE_GROUP": 32768,
        "CAN_BROADCAST": 65536, "MOD_ICON_VISIBLE": 131072, "IS_STAFF": 262144,
        "STAFF_ICON_VISIBLE": 524288
    }

    default_mod_flags = [64, 128, 256, 16384, 65536]
    default_mod_flags_value = 82368
    possible_mod_flags = [8, 64, 128, 256, 512, 8192, 16384, 65536, 262144]
    possible_mod_flags_value = 353224

    default_admin_flags = [4, 8, 16, 32, 64, 128, 256, 512, 1024, 8192, 16384,
                           65536, 262144]
    default_admin_flags_value = 354300
    admin_only_flags_value = 33846

    message_flags = {
        "PREMIUM": 4, "BG_ON": 8, "MEDIA_ON": 16, "CENSORED": 16,
        "SHOW_MOD_ICON": 64, "SHOW_STAFF_ICON": 128
    }

    channels = {
        "CHANNEL_RED": 256, "CHANNEL_ORANGE": 512, "CHANNEL_GREEN": 1024,
        "CHANNEL_CYAN": 2048, "CHANNEL_BLUE": 4096, "CHANNEL_PURPLE": 8192,
        "CHANNEL_PINK": 16384, "CHANNEL_MOD": 32768
    }

    nlp_warnings = {
        "LONGEST_REPEATING_SINGLE_MSG": 1, "LONGEST_REPEATING_MSG_IN_QUEUE": 2,
        "CHAR_VARIETY": 4, "MESSAGES_TOO_SHORT": 8, "NGRAM_GIBBERISH": 16
    }

    premium = {
        "INFO_NOT_AVAILABLE": 0, "NEVER_BEEN_PREMIUM": 100, "GIFTEE": 210,
        "PAYER": 200, "EXPIRED_GIFTEE": 111, "EXPIRED_PAYER": 101,
        "PENDING": -1
    }

    namechecker = {
        "ACCOUNT_AVAILABLE": 0, "NOT_A_USER": 1, "NAME_IS_GROUP": 2,
        "BAD_WORD": 4, "BAD_PARTS": 8, "NOT_EXPIRED": 16,
        "CONCURRENTLY_PURCHASED": 32, "ACTIVE_GROUP_OWNER": 64
    }

    purchase_error = {
        101: "First name is too long",
        102: "Last name is too long",
        103: "This card type is not supported",
        104: "The card number is too long",
        105: "The expiry date is incorrect",
        106: "The CVV code is incorrect",
        107: "Please check the amount",
        108: "This currency is not accepted",
        114: "User name is too long",
        115: "Password is too long",
        116: "Group name has too many characters",
        201: "Chatango does not accept payments from this card at this time",
        202: "You only recently started using this card and have made a " +
             "large number of purchases",
        203: "The maximum of {payments} payments in {days} has been exceeded"
    }

    announcement = {
        "IS_ENABLED": 1, "HAS_BG": 2
    }

    banlist_search = {
        "ERROR_TOO_LONG": "toolong", "ERROR_INVALID_USERNAME": "invaliduser",
        "ERROR_INVALID_IP": "invalidip", "RESULT_EMPTY": "empty",
        "RESULT_OK": "ok"
    }

def is_admin(flags: int) -> bool:
    assert type(flags) is int

    return int(flags, 10) & Flags.admin_only_flags_value

def is_default_mod(flags: int) -> bool:
    assert type(flags) is int

    return int(flags, 10) & Flags.default_mod_flags_value

def is_default_admin(flags: int) -> bool:
    assert type(flags) is int

    return int(flags, 10) & Flags.default_admin_flags_value

def parse_group_flags(flags: int) -> List[str]:
    assert type(flags) is int

    names = []

    for name, flag in Flags.group.items():
        if flags & flag:
            names.append(name)
    
    return names

def parse_mod_flags(flags: int) -> List[str]:
    assert type(flags) is int

    names = []

    for name, flag in Flags.mod.items():
        if flags & flag:
            names.append(name)
    
    return names

def parse_message_flags(flags: int) -> List[str]:
    assert type(flags) is int

    names = []

    for name, flag in Flags.message_flags.items():
        if flags & flag:
            names.append(name)
    
    return [*names, *get_channel_from_flags(flags)]

def get_channel_from_flags(flags: int) -> List[str]:
    assert type(flags) is int

    names = []

    for name, flag in Flags.channels.items():
        if flags & flag:
            names.append(name)
    
    return names

def parse_nlp_code(flags: int) -> List[str]:
    assert type(flags) is int

    names = []

    for name, flag in Flags.nlp_warnings.items():
        if flags & flag:
            names.append(name)
    
    return names

def parse_announcement(flags: int) -> List[str]:
    assert type(flags) is int

    names = []

    for name, flag in Flags.announcement.items():
        if flags & flag:
            names.append(name)

    return names

def parse_namechecker(flags: int) -> List[str]:
    assert type(flags) is int

    names = []

    for name, flag in Flags.namechecker.items():
        if flags & flag:
            names.append(name)
    
    return names

###

def build_group_flags(flags: List[str]) -> int:
    assert type(flags) is list

    ret_flag = 0

    for name in flags:
        try:
                ret_flag |= Flags.group[name.upper()]
        except:
            raise InvalidFlag(f"The supplied flag `{name}` is not valid")

    return ret_flag

def build_mod_flags(flags: List[str]) -> int:
    assert type(flags) is list

    ret_flag = 0

    for name in flags:
        try:
                ret_flag |= Flags.mod[name.upper()]
        except:
            raise InvalidFlag(f"The supplied flag `{name}` is not valid")

    return ret_flag

def build_message_flags(flags: List[str]) -> int:
    # TODO: implement this
    raise NotImplementedError()

def build_nlp_code(flags: List[str]) -> int:
    assert type(flags) is list

    ret_flag = 0

    for name in flags:
        try:
                ret_flag |= Flags.nlp_warnings[name.upper()]
        except:
            raise InvalidFlag(f"The supplied flag `{name}` is not valid")

    return ret_flag

def build_announcement(flags: List[str]) -> int:
    assert type(flags) is list

    ret_flag = 0

    for name in flags:
        try:
                ret_flag |= Flags.announcement[name.upper()]
        except:
            raise InvalidFlag(f"The supplied flag `{name}` is not valid")

    return ret_flag

def build_namechecker(flags: List[str]) -> int:
    assert type(flags) is list

    ret_flag = 0

    for name in flags:
        try:
                ret_flag |= Flags.namechecker[name.upper()]
        except:
            raise InvalidFlag(f"The supplied flag `{name}` is not valid")

    return ret_flag

def parse_premium(code: int) -> str:
    assert type(code) is int

    for name, _code in Flags.premium.items():
        if code == _code:
            return name

def get_purchase_error(code: Union[int, list]) -> str:
    assert type(code) in [int, list]

    return Flags.purchase_error.get(code, "An error occured, your account " +
                                          "has not been charged")
