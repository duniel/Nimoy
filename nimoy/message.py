#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

import html
import ipaddress
from types import MethodType

from .util import (parse_msg, expand_hex, compress_hex)

class Message(object):
    def __init__(self, name_color, font_size, font_face, font_color,
                 body, alias=None, timestamp=None, shortened_cookie=None,
                 encoded_cookie=None, msg_counter=None, msg_id=None,
                 ip_address=None, flags=None, **_bogus):
        self.name_color = name_color or "0"
        self.font_size  = int(font_size or 11)
        self.font_face  = font_face or "Arial"
        self.font_color = font_color or "0"
        self.body       = body
        self.alias      = alias

        # optional Chatango metadata
        self.timestamp        = float(timestamp) if timestamp else None
        self.shortened_cookie = int(shortened_cookie) if shortened_cookie \
                                                      else None
        self.encoded_cookie = encoded_cookie
        self.counter        = msg_counter
        self.id             = msg_id
        self.ip_address     = ipaddress.IPv4Address(ip_address) if ip_address \
                                                                else None
        self.flags = list(flags) if flags else None

    def __repr__(self):
        data = ""

        for k, v in self.__dict__.items():
            if k == "reply":
                continue

            data += f" {k}={v!r}"
        
        return f"<{self.__class__.__qualname__}{data}>"

    async def serialize(self, escape=True, *, bold=False, underline=False,
                        italic=False):
        substitution_map = dict(enumerate(("Arial Comic Georgia Handwriting "
                           "Impact Palatino Papyrus Times Typewriter").split()))
        # NOTE: flip the dictionary
        substitution_map = { v: k for k, v in substitution_map.items() }

        nc = compress_hex(self.name_color)
        fs = str(self.font_size).zfill(2)
        ff = self.font_face
        fc = compress_hex(self.font_color)

        if ff.capitalize() in substitution_map:
            ff = substitution_map[ff.capitalize()]

        if escape:
            body = self.body.encode("ascii", "xmlcharrefreplace")
            body = html.escape(body.decode())

        if nc == "0":
            nc = ""
        if bold:
            body = f"<b>{body}</b>"
        if underline:
            body = f"<u>{body}</u>"
        if italic:
            body = f"<i>{body}</i>"

        return f'<n{nc}/><f x{fs}{fc}="{ff}">{body}</f>'

    async def enable_reply(self, group):
        # HACK: refactor or remove this. at least clean the API up.
        async def r(self, *body):
            await group.post(f'@{self.alias} {" ".join(body)}')

        self.reply = MethodType(r, self)

    async def update_msg_id(self, msg_id, msg_counter=None):
        if msg_counter:
            # NOTE: if a counter is supplied, do a check
            if msg_counter == self.counter:
                self.id = msg_id
                return True
        else:
            self.id = msg_id
            return True

    @classmethod
    async def from_frame(cls, frame):
        body = html.unescape(frame.params["message"])

        return cls(**frame.params, **parse_msg(body))

    @classmethod
    async def parse(cls, raw_body):
        # NOTE: I need this "constructor" to auto-generate a `Message` instance
        # from a raw message.

        return cls(**parse_msg(html.unescape(raw_body)))
