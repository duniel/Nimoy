#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

__doc__ = """

"""

import time
import asyncio

from types     import FunctionType
from typing    import (Coroutine, Callable)
from traceback import format_exc

from .api       import get_group_profile
from .util      import (get_anon_id, generate_client_id)
from .user      import User
from .event     import Event
from .frame     import GroupFrame
from .message   import Message
from .tagserver import (get_host, get_id)
from .transport import Transport

def handle(frame_id: str) -> Callable:
    def _handle(func: Callable) -> Callable:
        func.frame_id = frame_id

        return func

    return _handle

class Group(object):
    def __init__(self, name: str, identity: User, callback: Coroutine=None,
                 server: str="", port: int=8080, pull_history: bool=False,
                 discard_climit: bool=True, frame_factory=GroupFrame) -> None:

        self.name     = name.lower()
        self.identity = identity
        self.callback = callback

        self.port            = port or 8081
        self.server          = server or get_host(name)
        self.transport       = None
        self._handlers       = list()
        self.pull_history    = pull_history
        self.frame_factory   = frame_factory
        self.server_number   = get_id(name)
        self._climit_queue   = list()
        self._discard_cqueue = True

        # unpopulated - font styles
        self.name_color = None
        self.font_color = None
        self.font_size  = None
        self.font_face  = None

        # unpopulated - chatango metadata
        self.bans                 = list()
        self.users                = set()
        self.posts                = list()
        self.owner                = None
        self.flags                = list()
        self.title                = str()
        self.unbans               = list()
        self.silent               = False
        self.own_ip               = None
        self.premium              = "UNKNOWN"
        self.history              = list()
        self._nomore              = False
        self.own_name             = None
        self.rate_limit           = int()
        self.moderators           = set()
        self.session_id           = None
        self.user_count           = int()
        self.anon_count           = int()
        self.description          = str()
        self.has_premium          = None
        self.mod_actions          = set()
        self.current_name         = str()
        self.banned_words         = set()
        self.connect_timestamp    = None
        self.rate_limit_timeout   = int()
        self.premium_expiration   = None
        self.partial_banned_words = set()

    # here be dragons

    def __repr__(self):
        # repr(group)
        return (f"<object {self.__class__.__qualname__} ({self.server_number}:"
                f" {self.name!r}) at {hex(id(self))!s}>")

    # str(group)
    __str__ = __repr__

    def __contains__(self, user):
        # user in group
        assert isinstance(user, User)

        return user.username in self.users

    def __iter__(self):
        # for user in group: f(user)
        return iter(self.users)

    # end of dragons

    @property
    def connected(self):
        return self.transport.open

    @classmethod
    async def join(cls, *args, **kwargs):
        g = cls(*args, **kwargs)

        await g.connect()

        return g

    async def leave(self):
        if self.connected:
            await self.transport.disconnect()
            return True

        return False

    async def connect(self):
        try:
            # NOTE: default is SSL and port 8081
            self.transport = await Transport.create_connection(self.server)

            await self._setup_handlers()
            await self.send_frame(self.frame_factory("v"))

            asyncio.ensure_future(self._pinger())
            asyncio.ensure_future(self._main())
        except Exception as e:
            await self._emit(Event("error", exception=e, fatal=True,
                                    traceback=format_exc()))

    async def post(self, body, escape=True):
        m = Message(
            alias=self.identity.username,
            name_color=self.name_color or self.identity.styles["nameColor"],
            font_color=self.font_color or self.identity.styles["textColor"],
            font_size= self.font_size  or int(self.identity.styles["fontSize"]),
            font_face= self.font_face  or self.identity.styles["fontFamily"],
            body=body
        )

        await self._post(await m.serialize())
        await self._emit(Event("sent_message", message=m))

    async def _post(self, raw):
        await self.send_frame(self.frame_factory("bm", body=raw))

    async def send_frame(self, frame):
        # NOTE: alias purposes
        await self.transport.send_frame(frame)
        await self._emit(Event("sent_frame", frame=frame))

    async def send_data(self, *data):
        # NOTE: alias purposes
        await self.transport.send_data(*data)
        await self._emit(Event("sent_data", data=data))

    async def authenticate(self):
        f = self.frame_factory("bauth",
            group=   self.name,
            username=self.identity.username,
            password=self.identity.password
        )

        await self._emit(Event("sent_auth", frame=f))
        await self.send_frame(f)

    async def _emit(self, event):
        await self.callback(self, event)

    async def _pinger(self):
        while self.connected:
            await asyncio.sleep(30)
            await self.send_frame(self.frame_factory())
            await self._emit(Event("sent_ping")) 

    async def _main(self):
        while self.connected:
            data  = await self.transport.recv()
            await self._emit(Event("received_data",  data=data))

            frame = await self.frame_factory.parse(data)
            await self._emit(Event("received_frame", frame=frame))

            await self._process(frame)

    async def _setup_handlers(self):
        for attribute in dir(self):
            func = getattr(self, attribute)

            if callable(func) and hasattr(func, "frame_id"):
                self._handlers.append(func)

    async def _get_handlers(self):
        return { f.__qualname__: f for f in self._handlers }

    async def _process(self, frame):
        assert isinstance(frame, self.frame_factory)

        for handler in self._handlers:
            #if type() # TODO
            if handler.frame_id == frame.cmd:
                event = await handler(frame)

                if isinstance(event, Event):
                    await self._emit(event)

    @handle("")
    async def _received_ping(self, frame):
        return Event("received_ping")

    @handle("v")
    async def _received_version(self, frame):
        await self.authenticate()

        return Event("received_version", frame=frame)

    @handle("ok")
    async def _received_ok(self, frame):
            self.owner      = frame["owner"]
            self.session_id = frame["session_id"]
            self.own_name   = frame["own_username"]
            self.moderators = frame["mods"]
            self.flags      = frame["flags"]

            # NOTE: for anon ID generation
            self.connect_timestamp = ts = frame["timestamp"]

            if frame["type"] == "N" and self.identity.username \
                                        and self.identity.password:
                # TODO: refactor this!
                # NOTE: 4 last chars of the ts without precision
                nc              = str(ts).split(".")[0][-4:]
                aid             = frame["session_id"][:8]
                self.name_color = nc
                self.own_name   = "anon" + get_anon_id(nc, aid)

                # TODO: implement this
                await self._emit(Event("logged_in"))

            elif frame["type"] == "N" and self.identity.password == None:
                # TODO: implement this
                # joined as name with no password
                await self._emit(Event("logged_in"))

            elif frame["type"] == "M":
                await self._emit(Event("invalid_login", group=self.name,
                                        identity=self.identity))

            if "RATELIMITREGIMEON" in self.flags:
                await self.send_frame(self.frame_factory("getratelimit"))

            if "HAS_XML" in self.flags:
                info = await get_group_profile(self.name)

                self.title       = info["title"]
                self.description = info["description"]

                await self._emit(Event("got_group_profile", title=self.title,
                                       description=self.description))

    @handle("inited")
    async def _handle_initialized(self, frame):
            # do the things
            # NOTE: get premium status for itself
            await self.send_frame(self.frame_factory("getpremium"))
            # NOTE: receive the participant list and anons - this works when the
            #       counter is disabled.
            await self.send_frame(self.frame_factory("gparticipants"))
            if not self._nomore and self.pull_history:
                await self.send_frame(self.frame_factory("get_more",
                                      batch_size=50, page=0))

            return Event("initialized")

    @handle("premium")
    async def _got_premium(self, frame):
        self.premium            = frame["status"]
        self.has_premium        = frame["has_premium"]
        self.premium_expiration = frame["timestamp"]

        # TODO
        if self.has_premium: # and self._use_bg:
            # NOTE: enable background, twice for some reason
            await self.send_frame(self.frame_factory("msgbg", mode=True))
            await self.send_frame(self.frame_factory("msgbg", mode=True))

        return Event("premium", frame=frame)

    @handle("nomore")
    async def _no_more_history(self, frame):
        self._nomore = True

        Event("no_more_history")

    @handle("gotmore")
    async def _got_more_history(self, frame):
        # NOTE: get all the group history. Change this?
        next_page = frame["page"] + 1
        await self.send_frame(self.frame_factory("get_more",
                              batch_size=50, page=next_page))

        return Event("more_history", frame=frame)

    @handle("denied")
    async def _denied_login(self, frame):
        return Event("denied")

    @handle("gparticipants")
    async def _group_participants(self, frame):
        self.users      = frame["participants"]
        self.anon_count = frame["anons"]

        return Event("initial_userlist", users=self.users,
                     anon_count=self.anon_count)

    @handle("participant")
    async def _participant_change(self, frame):
        # TODO: implement this
        pass

    @handle("bw")
    async def _got_banned_words(self, frame):
        self.banned_words = frame["banned_words"]

        return Event("banned_words", words=self.banned_words)

    @handle("getratelimit")
    async def _got_rate_limit(self, frame):
        self.rate_limit         = frame["limit"]
        self.rate_limit_timeout = frame["time_left"]

        return Event("rate_limit", rate_limit=self.rate_limit,
                     rate_limit_timeout=self.rate_limit_timeout)

    @handle("ratelimited")
    async def _rate_limited(self, frame):
        self.rate_limit_timeout = frame["time_left"]

        return Event("rate_limited", timeout=self.rate_limit_timeout)

    @handle("mods")
    async def _got_modlist(self, frame):
        self.moderators = frame["moderators"]

        return Event("moderator_list", moderators=self.moderators)

    @handle("n")
    async def _got_user_count(self, frame):
        self.user_count = frame["count"]

        return Event("user_count", count=self.user_count)

    @handle("i")
    async def _initial_message(self, frame):
        msg = await Message.from_frame(frame)

        # NOTE: make `msg.reply` available
        await msg.enable_reply(self)

        self.history.append(msg)

        return Event("initial_message", message=msg)

    @handle("b")
    async def _live_message(self, frame):
        msg = await Message.from_frame(frame)

        # NOTE: make `msg.reply` available
        await msg.enable_reply(self)

        self.history.append(msg)

        return Event("message", message=msg)

    @handle("u")
    async def _got_permanent_id(self, frame):
        for msg in self.history:
            if await msg.update_msg_id(frame["msg_id"], frame["msg_counter"]):
                return Event("got_msgid", id=frame["msg_id"],
                             counter=frame["msg_counter"], message=msg)

    @handle("show_fw")
    async def _got_flood_warning(self, frame):
        return Event("flood_warning")

    @handle("show_tb")
    async def _show_timeban(self, frame):
        return Event("time_banned", remaining=frame["remaining"])

    @handle("show_tb")
    async def _timebanned(self, frame):
        return Event("time_banned", remaining=frame["remaining"])

    @handle("deleteall")
    async def _delete_several_messages(self, frame):
        for msg_id in frame["message_ids"]:
            for msg in self.history:
                if msg.id == msg_id:
                    self.history.remove(msg)

                    await self._emit(Event("message_deleted", message=msg))

    @handle("delete")
    async def _delete_one_message(self, frame):
        for msg in self.history:
            if msg.id == frame["message_id"]:
                self.history.remove(msg)

                await self._emit(Event("message_deleted", message=msg))

    @handle("clearall")
    async def _clearall_msgs(self, frame):
        self.history = list() # TODO: *don't* clear the history?

        return Event("messages_cleared")

    @handle("climited")
    async def _command_limited(self, frame):
        limited_frame = self.frame_factory(*frame["arguments"])

        if not self._discard_cqueue:
            self._climit_queue.append(limited_frame)
