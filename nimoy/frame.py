#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

from __future__ import annotations

import sys
import json
import time
import ipaddress

from urllib.parse import unquote as url_unquote
from urllib.parse import quote   as url_quote

from typing import (Callable, List)

from .util  import  (generate_client_id, generate_session_id)
from .error import (MissingFrame, WrongVersion)
from .flag  import  (parse_mod_flags, parse_group_flags, parse_premium,
                     parse_message_flags, parse_nlp_code, parse_announcement)

def command(kind: str, command: str, aliases: list=[]) -> Callable:
    assert type(kind) is str
    assert type(command) is str
    assert kind in ["server", "client"]

    def wrapper(f):
        f.kind = kind
        f.command = command
        if not getattr(f, "aliases", False):
            f.aliases = []
        f.aliases.extend(aliases)

        return f

    return wrapper

class __MetaFrame(type):
    # to register command handlers / generators
    def __init__(cls, name, bases, params):
        # TODO: refactor this mess (`__MetaFrame`)

        async def server_ping():     pass
        async def client_ping(self): pass
        server_ping.kind = "server"
        client_ping.kind = "client"

        cls.listeners = { "": server_ping }
        cls.commands  = { "": client_ping }
        cls.aliases = {
            "server": {},
            "client": {}
        }

        for k, v in params.items():
            if callable(v) and getattr(v, "kind", False) and not "__" in k:
                aliases = getattr(v, "aliases", [])
                if v.kind == "server":
                    cls.listeners[v.command] = v

                elif v.kind == "client":
                    cls.commands[v.command] = v

                if aliases:
                    for alias in aliases:
                        cls.aliases[v.kind][alias] = v.command

class __Frame(object, metaclass=__MetaFrame):
    def __init__(self, cmd: str="", **kwargs) -> None:
        assert type(cmd) is str

        self.cmd = cmd
        self.params = kwargs

# TODO: remove or fix `__Frame.__new__`
#       def __new__(cls, *args, **kwargs):
#         if cls is Frame:
#             raise TypeError("{} may not be instantiated directly"
#                             .format(cls.__class__.__qualname__))
#         return object.__new__(cls, *args, **kwargs)

    def __getitem__(self, identifier):
        return self.params[identifier]

    def __repr__(self):
        data = ""

        if self.params:
            data = ":"

        for k, v in self.params.items():
            data += f" {k}={v!r}"

        return f"<{self.__class__.__qualname__} {self.cmd!r}{data}>"

    @classmethod
    async def parse(cls, raw: str) -> __Frame:
        # NOTE: do NOT check if `args' is present, a ping is only \r\n
        assert type(raw) is str

        cmd, *args = raw.strip("\r\n\0").split(":")

        real_cmd = cmd

        if cmd in cls.aliases["server"]:
            real_cmd = cls.aliases["server"][cmd]

        # Comment from Chatango source code:
        #   STRIP off empty array items
        #   some commands come in with a colon after them - no matter if they
        #   are followed by arguments
        #   e.g "blocklist:" or "blocklist:a20"
        #   both of these would produce an arr of length 2
        #   so catch this case and strip off the last item

        #if len(args) == 2 and not args[1]:
        #    args = args[0]
        if len(args) > 0:
            while raw and not args[-1]:
                args.pop()

        try:
            # get handler for `_cmd'
            handler = cls.listeners[real_cmd]
            assert callable(handler)
            # TODO: `__Frame.parse()`: move the following logic into a separate `try` block call handler, should return a dict
            data =  (await handler(*args)) or {}
            assert type(data) is dict
            # create frame instance
            frame = cls(cmd, **data)
            # returns the frame if there are no exceptions
            return frame
        except KeyError:
            raise MissingFrame(f"Frame handler for command {cmd!r} does not "
                                                            "exist") # from None
        except Exception:
            # re-raise any exceptions in the handler itself
            raise

    async def dump(self) -> List[str]:
        assert type(self.params) is dict

        _cmd = cmd = self.cmd

        # pylint: disable=E1101
        if cmd in self.aliases["client"]:
            _cmd = self.aliases["client"][cmd]

        try:
            # get generator for `_cmd'
            # pylint: disable=E1101
            generator = self.commands[_cmd]
            assert callable(generator)
            # get the command list from the generator
            # TODO: `__Frame.dump`: move the following logic into a separate `try` block
            data = (await generator(self)) or []
            # make sure it's a list and not a string
            if type(data) is str:
                data = [ data ]
            assert type(data) is list
            # return the [cmd, generated_data] list
            return [cmd, *data]
        except KeyError:
            # raise own error and remove the KeyError context
            raise MissingFrame(f"Frame generator for command {cmd} does not "
                                                            "exist") # from None
        except Exception:
            # re-raise any exceptions in the generator itself
            raise

# TODO: `PingFrame`: implement? remove?
# class PingFrame(object):
#     def __init__(self, *args, **kwargs):
#         self.cmd = "$ping"
#         self.params = {}
    
#     async def dump(self):
#         return

#     async def parse(self):
#         return

class MCFrame(__Frame):
    #####
    # Message Catcher tagserver commands

    @command("client", "version")
    async def send_version(self):
        """ version:low:high """

        return [
            self.params.get("low", "4"),
            self.params.get("high", "1")
        ]

    @command("server", "OK")
    # pylint: disable=E0211
    async def OK():
        pass

    @command("client", "login")
    # pylint: disable=E0213
    async def login(self):
        return [
            self["username"],
            self["password"]
        ]

    @command("server", "msgcount")
    # pylint: disable=E0213
    async def msgcount(num, *read):
        """ msgcount:num:[read] """
        return {
            "num": int(num),
            **( { "read": True } if read else {} )
        }

    @command("server", "msg")
    # pylint: disable=E0213,E1101
    async def msg(uid, from_, idk_, *msg):
        """ msg:from:?:?:msg """
        return {
            "from": from_,
            "uid":  uid if uid.isdigit() else None,
            "msg":  ":".join(msg),
            "type": "user" if from_ else "anon"
        }

class GroupFrame(__Frame):
    #####
    # Group tagserver commands

    ### START VERSION

    @command("client", "v")
    async def send_version(self):
        pass

    @command("server", "v")
    # pylint: disable=E0213
    async def recv_version(low, high):
        """ v:low:high """

        return {
            "low": int(low),
            "high": int(high)
        }

    @command("server", "versioningPU")
    # pylint: disable=E0211
    async def versioningPU():
        raise WrongVersion("current version is too low")

    @command("server", "imsg")
    # pylint: disable=E0211
    async def imsg(*args):
        raise WrongVersion("the command `imsg' is deprecated")

    ### END VERSION

    ### START AUTHENTICATION

    @command("client", "bauth")
    async def bauth(self):
        """ bauth:<group>:[session_id]:[username]:[password] """

        return [
            self["group"], self.params.get("session_id", ""),
            self.params.get("username", ""), self.params.get("password", "")
        ]

    @command("server", "ok")
     # pylint: disable=E0213
    async def ok(owner, sid, type_, ownuser, timestamp, ownip, mods_, flags):
        """ ok:owner:session_id:type:ownuser:timestamp:ownip:[mods]:flags """

        mods = []

        if mods_:
            # get list of mod;flag
            for mod in mods_.split(";"):
                # separate mod and flag
                mod, flag = mod.split(",")

                mods.append({
                    "alias": mod.lower(),
                    "flags": parse_mod_flags(int(flag))
                })

        return {
            "owner": owner,
            "session_id": int(sid),
            "type": type_, # N = valid login, M = invalid login, C = ?
            "own_username": ownuser,
            "timestamp": float(timestamp),
            "own_ip": ipaddress.ip_address(ownip),
            "mods": mods,
            "flags": parse_group_flags(int(flags))
        }

    @command("server", "badlogin", aliases=[ "badalias" ])
    # pylint: disable=E0213
    async def badlogin(reason):
        if reason == "1":
            reason = "BANNED_WORDS"
        else:
            reason = "NAME_TAKEN"

        return { "reason": reason }

    @command("server", "logoutok")
    # pylint: disable=E0211
    async def logoutok():
        pass

    @command("server", "denied")
    # pylint: disable=E0211
    async def denied():
        pass

    @command("server", "inited")
    # pylint: disable=E0211
    async def inited():
        pass

    @command("server", "mustlogin")
    # pylint: disable=E0211
    async def mustlogin():
        pass

    @command("server", "aliasok")
    # pylint: disable=E0211
    async def aliasok():
        pass

    @command("server", "pwdok")
    # pylint: disable=E0211
    async def pwdok():
        pass

    @command("client", "blogout")
    async def blogout(self):
        pass

    @command("client", "reload_init_batch")
    async def reload_init_batch(self):
        pass

    ### END AUTHENTICATION

    ### START PREMIUM

    @command("client", "getpremium")
    # pylint: disable=E0211
    async def getpremium(self):
        return

    @command("server", "premium")
    # pylint: disable=E0213
    async def premium(status, timestamp):
        return {
            "status": parse_premium(int(status)),
            "timestamp": float(timestamp),
            "has_premium": float(timestamp) > time.time()
        }

    @command("server", "updateprofile")
    # pylint: disable=E0213
    async def updateprofile(alias):
        return {
            "alias": alias
        }

    @command("server", "mtoken")
    # pylint: disable=E0213
    async def media_token(token):
        return { "media_token": token }

    @command("server", "miu")
    # pylint: disable=E0213
    async def message_img_update(target):
        return { "target": target }

    @command("client", "msgbg")
    # pylint: disable=E0211
    async def msg_background(self):
        return "1" if self["mode"] else "0"

    @command("client", "msgmedia")
    # pylint: disable=E0211
    async def msg_media(self):
        return "1" if self["mode"] else "0"

    ### END PREMIUM

    ### START PARTICIPANTS

    @command("server", "n")
    # pylint: disable=E0213
    async def user_count(count):
        return { "count": int(count, 16) }

    @command("client", "gparticipants")
    async def send_gparticipants(self):
        if self.params.get("stop"):
            return "stop"

    @command("server", "gparticipants")
    # pylint: disable=E0213
    async def recv_gparticipants(anons, *participants):
        if participants:
            # Append ":" to fix a bug in the implementation
            participants = (":".join(participants) + ":").split(";")

        users = []

        for parti in participants:
            parti = parti.split(":")

            if parti[5]:
                ip_address = ipaddress.ip_address(parti[5])
            else:
                ip_address = None

            if parti[4] == "None":
                parti[4] = None

            users.append({
                "session_id": int(parti[0]),
                "timestamp": float(parti[1]),
                "shortened_cookie": int(parti[2]),
                "username": parti[3],
                "alias": parti[4],
                "ip_address": ip_address
            })

        return {
            "anons": int(anons),
            "participants": users
        }

    @command("server", "participant")
    # pylint: disable=E0213
    async def participant(kind, session_id, shortened_cookie, username, alias,
                    ip_address, timestamp):
        # (name, session_id, timestamp, cookie, alias, ip):
        kind = int(kind)
        if kind == 0:
            kind = "leave"
        elif kind == 1:
            kind = "join"
        else:
            kind = "change"

        if ip_address:
            ip_address = ipaddress.ip_address(ip_address)
        else:
            ip_address = None

        return {
            "kind": kind,
            "participant": {
                "session_id": int(session_id),
                "timestamp": float(timestamp),
                "shortened_cookie": int(shortened_cookie),
                "username": username,
                "alias": alias,
                "ip_address": ip_address
            }
        }

    @command("server", "g_participants")
    # pylint: disable=E0213
    async def recv_g_participants(todo):
        pass # TODO: implement `GroupFrame` 'g_participants' server command
    
    @command("client", "profupdated")
    async def profupdated(self):
        pass # XXX: deprecated

    ### END PARTICIPANTS

    ### START BANNED_WORDS

    @command("server", "bw")
    # pylint: disable=E0211
    async def banned_words(*bw):
        bw = list(bw)

        if bw:
            bw = bw[0].split("%2C")

        return { "banned_words": bw }

    @command("server", "ubw")
    # pylint: disable=E0211
    async def unbanned_words():
        pass

    @command("server", "cbw")
    # pylint: disable=E0211
    async def changed_banned_words(*idk):
        pass # TODO: `GroupFrame`: implement server command 'cbw'

    @command("client", "getbannedwords")
    # pylint: disable=E0211
    async def getbannedwords():
        pass
    
    @command("client", "setbannedwords")
    async def setbannedwords(self):
        # TODO: `GroupFrame`: server command 'setbannedwords': words and partial words, null byte (\0) at the start???
        raise NotImplementedError()

        the_words = ",".join(self["words"])

        return url_quote(the_words)

    ### END BANNED_WORDS

    ### START MESSAGES

    @command("client", "get_more")
    async def get_more(self):
        return [
            self.params.get("batch_size", 20),
            self["page"]
        ]

    @command("client", "bm")
    async def send_h5_msg(self):
        return [
            self.params.get("client_id", generate_client_id()),
            self.params.get("flags", "0"),
            self["body"]
        ]

    @command("server", "gotmore")
    # pylint: disable=E0213
    async def gotmore(page):
        return { "page": int(page) }

    @command("server", "nomore")
    # pylint: disable=E0211
    async def nomore(): pass

    @command("server", "b")
    # pylint: disable=E0213
    async def live_message(timestamp, alias, temp_name, shortened_cookie,
                     encoded_cookie, msg_counter, ip_address, flags, _bogus,
                     *message):

        if ip_address:
            ip_address = ipaddress.IPv4Address(ip_address)
        else:
            ip_address = None

        return {
            "timestamp": float(timestamp),
            "alias": alias,
            "temp_name": temp_name or None,
            "shortened_cookie": int(shortened_cookie),
            "encoded_cookie": encoded_cookie or None,
            "msg_counter": msg_counter,
            "ip_address": ip_address,
            "flags": parse_message_flags(int(flags)), # includes channel
            "message": ":".join(message)
        }

    @command("server", "i")
    # pylint: disable=E0213
    async def history_message(timestamp, alias, temp_name, shortened_cookie,
                     encoded_cookie, msg_id, ip_address, flags, _bogus,
                     *message):
        # NOTE: I wish I could alias "i" to "u", but this uses `msg_id` and
        # not `msg_counter` since it already is inserted into the DB.

        if ip_address:
            ip_address = ipaddress.IPv4Address(ip_address)
        else:
            ip_address = None

        return {
            "timestamp": float(timestamp),
            "alias": alias,
            "temp_name": temp_name or None,
            "shortened_cookie": int(shortened_cookie),
            "encoded_cookie": encoded_cookie or None,
            "msg_id": msg_id,
            "ip_address": ip_address,
            "flags": parse_message_flags(int(flags)), # includes channel
            "message": ":".join(message)
        }

    @command("server", "u")
    # pylint: disable=E0213
    async def msg_db_id(msg_counter, msg_id):
        return {
            "msg_counter": msg_counter,
            "msg_id": msg_id
        }

    @command("server", "chatango")
    # pylint: disable=E0213
    async def system_msg(message):
        return { "message": message }

    @command("server", "delete")
    # pylint: disable=E0213
    async def delete_msg(msg_id):
        return { "message_id": msg_id }

    @command("server", "deleteall")
    # pylint: disable=E0211
    async def delete_many_msg(*msgids):
        return { "message_ids": list(msgids) }

    @command("server", "clearall")
    # pylint: disable=E0211
    async def recv_clearall():
        pass
    
    @command("client", "g_flag")
    async def flagmsg(self):
        return self["message_id"]

    @command("client", "delmsg")
    async def send_delete_message(self):
        return self["message_id"]

    @command("client", "delallmsg")
    async def delallmsg(self):
        name = self.params.get("username")
        if name:
            # HACK
            name = [name]

        return [
            self["message_id"],
            self["ip_address"],
            *( name or [] ) # HACK
        ]
    
    @command("client", "delete")
    async def send_delete_msg_2_what(self):
        # TODO: `GroupFrame`: server command 'delete': figure out what 'delete' vs 'delmsg' means
        return self["message_id"]

    @command("client", "clearall")
    async def send_clearall(self):
        pass

    ### END MESSAGES

    ### START RATE_LIMIT

    @command("server", "getratelimit")
    # pylint: disable=E0213
    async def recv_getratelimit(limit, time_left):
        return {
            "limit": int(limit),
            "time_left": int(time_left)
        }
    
    @command("server", "ratelimited")
    # pylint: disable=E0213
    async def ratelimited(time_left):
        return { "time_left": int(time_left) }

    @command("server", "ratelimitset")
    # pylint: disable=E0213
    async def ratelimitset(limit):
        return { "limit": int(limit) }

    @command("server", "show_fw")
    # pylint: disable=E0211
    async def show_fw():
        pass

    @command("server", "show_tb")
    # pylint: disable=E0213
    async def show_tb(remaining):
        return { "remaining": int(remaining) }

    @command("server", "tb")
    # pylint: disable=E0213
    async def timeban(remaining):
        return { "remaining": int(remaining) }

    @command("server", "end_fw")
    # pylint: disable=E0211
    async def end_fw():
        pass

    @command("server", "show_nlp")
    # pylint: disable=E0213
    async def show_nlp(reason):
        return { "reason": parse_nlp_code(int(reason)) }

    @command("server", "show_nlp_tb")
    # pylint: disable=E0213
    async def show_nlp_tb(reason, remaining):
        return {
            "reason": parse_nlp_code(int(reason)),
            "remaining": int(remaining)
        }

    @command("server", "nlptb")
    # pylint: disable=E0213
    async def nlptb(remaining):
        return { "remaining": int(remaining) }

    @command("server", "climited")
    # pylint: disable=E0213
    async def climited(cmd, *args):
        return {
            "command": cmd,
            "arguments": list(args)
        }
    
    @command("server", "proxybanned")
    # pylint: disable=E0211
    async def proxybanned():
        pass # blocked brazilian IP or known server block IP

    @command("server", "verificationchanged")
    # pylint: disable=E0211
    async def verificationchanged():
        pass

    @command("server", "verificationrequired")
    # pylint: disable=E0211
    async def verificationrequired():
        # NOTE: on email change and verified, check /updateprofile
        pass

    @command("server", "limitexceeded")
    # pylint: disable=E0211
    async def limitexceeded():
        pass # NOTE: > 26 connections from one IP
    
    @command("server", "msglexceeded")
    # pylint: disable=E0211
    async def msglexceeded():
        pass # NOTE: message length exceeded

    @command("client", "getratelimit")
    async def send_getratelimit(self):
        pass

    ### END RATE_LIMIT

    ### START MOD_MANAGEMENT

    @command("server", "mods")
    # pylint: disable=E0211
    async def mods(*mods_):
        if not mods_:
            return { "moderators": [] }

        mods = []
        for mod in mods_:
            mod, flag = mod.split(",")
            mods.append({
                "alias": mod.lower(),
                "flags": parse_mod_flags(int(flag))
            })

        return { "moderators": mods }

    @command("server", "addmoderr")
    # pylint: disable=E0213
    async def addmoderr(reason, errcode=None):
        pass # TODO: implement `GroupFrame.addmoderr`

    @command("server", "removemoderr")
    # pylint: disable=E0211
    async def removemoderr(*what):
        pass # TODO: implement `GroupFrame.removemoderr`

    @command("server", "updatemoderr")
    # pylint: disable=E0211
    async def updatemoderr(*what):
        pass # TODO: implement `GroupFrame.updatemoderr`
    
    @command("server", "modactions")
    # pylint: disable=E0211
    async def modactions(*actions):
        return # TODO: implement `GroupFrame.modactions`
        actions = ":".join(actions).split(";")
        
        action_list = []
        for action in actions:
            action = action.split(",")
            if action[3]:
                # NOTE: parse IP
                action[3] = ipaddress.IPv4Address(action[3])
            
            if action[4] == "None":
                # NOTE: stupid server-side serialization
                action[4] = None
            
            if action[6]:
                action[6] = ",".join(action[6:])
                del action[7:]
                action[6] = url_unquote(action[6])
                action[6] = json.loads(action[6])

            action_list.append({
                "id": action[0],
                "type": action[1],
                "mod_alias": action[2].lower(),
                "mod_ip": action[3],
                "target_alias": action[4],
                "timestamp": float(action[5]),
                "json": action[6]
            })

        return { "actions": action_list }

    @command("client", "addmod", aliases=[ "updmod" ])
    async def addmod(self):
        flags = self.params.get("flags")
        if flags:
            # HACK
            flags = [ flags ]

        return [
            self["username"],
            *( flags or [] ) # HACK
        ]

    # # TODO: deprecate `GroupFrame.updmod` in favor of the alias above? test it!
    # @command("client", "updmod")
    # async def updmod(self):
    #     flags = self.params.get("flags")
    #     if flags:
    #         # HACK
    #         flags = [ flags ]

    #     return [
    #         self["username"],
    #         *( flags or [] ) # HACK
    #     ]

    @command("client", "removemod")
    async def removemod(self):
        return self["username"]

    @command("client", "getmodactions")
    async def getmodactions(self):
        # TODO: `GroupFrame.getmodactions`: figure out the API and be certain before implementing
        raise NotImplementedError()

    ### END MOD_MANAGEMENT

    ### START BAN_MANAGEMENT

    @command("server", "blocklist")
    # pylint: disable=E0211
    async def recv_blocklist(*thelist):
        pass # TODO: implement `GroupFrame.recv_blocklist`

    @command("server", "unblocklist")
    # pylint: disable=E0211
    async def unblocklist(*thelist):
        pass # TODO: implement `GroupFrame.unblocklist`

    @command("server", "badbansearchstring")
    # pylint: disable=E0213
    async def badbansearchstring(TODO):
        pass # TODO: implement `GroupFrame.badbansearchstring`

    @command("server", "bansearchresult")
    # pylint: disable=E0213
    async def bansearchresult(TODO):
        pass # TODO: implement `GroupFrame.bansearchresult`

    @command("server", "blocked", aliases=[ "unblocked" ])
    # pylint: disable=E0213
    async def blocked(encoded_cookie, ip_address, by_mod, timestamp):
        return {
            "encoded_cookie": encoded_cookie,
            "ip_address": ipaddress.IPv4Address(ip_address),
            "by_mod": by_mod.lower(),
            "timestamp": float(timestamp)
        }

    # TODO: deprecate `GroupFrame.unblocked` in favor of the alias above? test it!
    # @command("server", "unblocked")
    # # pylint: disable=E0213
    # async def unblocked(encoded_cookie, ip_address, by_mod, timestamp):
    #     return {
    #         "encoded_cookie": encoded_cookie,
    #         "ip_address": ipaddress.IPv4Address(ip_address),
    #         "by_mod": by_mod.lower(),
    #         "timestamp": float(timestamp)
    #     }
    
    @command("client", "block")
    async def ban_user(self):
        username = self.params.get("username")
        if username:
            # HACK
            username = [username]
        
        return [
            self["encoded_cookie"],
            self["ip_address"],
            *( username or [] ) # HACK
        ]
    
    @command("client", "removeblock")
    async def removeblock(self):
        return [
            self["encoded_cookie"],
            self["ip_address"]
        ]

    @command("client", "searchban")
    async def searchban(self):
        return self["username"]

    @command("client", "blocklist")
    async def send_blocklist(self):
        # TODO: `GroupFrame.send_blocklist`: figure out the API and be certain before implementing
        raise NotImplementedError()

        return [
            "block",
            self["timestamp"],
            "next",
            self["request_count"],
            "anons",
            self.params.get("anons", 0)
        ]

    ### END BAN_MANAGEMENT

    ### START NOTIFICATIONS

    @command("server", "notifysettings")
    # pylint: disable=E0213
    async def notifysettings(email, what1, what2, what3):
        pass # TODO: implement `GroupFrame.notifysettings`

    @command("server", "checkemail_notify")
    # pylint: disable=E0213
    async def checkemail_notify(idk):
        pass # TODO: implement `GroupFrame.checkemail_notify`

    @command("client", "getnotifysettings")
    async def getnotifysettings(self):
        pass # TODO: implement `GroupFrame.getnotifysettings`

    ### END NOTIFICATIONS

    ### START MISCELLANEOUS

    @command("server", "groupflagstoggled", aliases=[ "groupflagsupdate" ])
    # pylint: disable=E0213
    async def groupflagstoggled(flags):
        return { "flags": parse_group_flags(int(flags)) }

    @command("server", "updgroupinfo")
    # pylint: disable=E0213
    async def updgroupinfo(owner_message, description):
        return {
            "owner_message": url_unquote(owner_message),
            "description": url_unquote(description)
        }

    @command("server", "annc")
    # pylint: disable=E0213
    async def announcement(flags, name=None, *text):
        if flags != "none":
            return {
                "flags": parse_announcement(int(flags)),
                "name": name,
                "text": ":".join(text)
            }

    @command("server", "getannc")
    # pylint: disable=E0213
    async def get_announcement_server(flags, name=None, delay=None,
                                      periodicity=None, text=None):

        if flags != "none":
            return {
                "flags": parse_announcement(int(flags)),
                "name": name,
                "delay": int(delay),
                "periodicity": int(periodicity),
                "text": text
            }

    @command("client", "getannouncement")
    async def get_announcement_client(self):
        pass

    ### END MISCELLANEOUS

class PMFrame(__Frame):
    #####
    # PM tagserver commands

    @command("server", "OK", aliases=[ "kickingoff", "firstlogin", "show_fw",
                                                    "DENIED", "lowversion" ])
    # pylint: disable=E0211
    async def OK():
        pass
    
    @command("server", "seller_name")
    # pylint: disable=E0213
    async def seller_name(alias, session_id):
        return {
            "alias": alias,
            "session_id": int(session_id)
        }

    # TODO: deprecate `PMFrame.kickingoff` and `PMFrame.firstlogin` in favor of the alias to `PMFrame.OK`?
    # @command("server", "kickingoff")
    # # pylint: disable=E0211
    # async def kickingoff():
    #     pass # PM connected as the same username elsewhere

    # @command("server", "firstlogin")
    # # pylint: disable=E0211
    # async def firstlogin():
    #     pass
    
    @command("server", "time")
    # pylint: disable=E0213
    async def server_time(timestamp):
        # server side time for delta
        return { "timestamp": float(timestamp) }

    @command("server", "connect")
    # pylint: disable=E0213
    async def connect(alias, idle, status):
        # NOTE: status = app|online|offline|nouser|invalid]
        return {
            "alias": alias,
            "idle": int(idle) if idle != "None" else 0, # TODO: `PMFrame.connect` "idle" error handling?
            "status": status
        }

    @command("server", "reload_profile")
    # pylint: disable=E0213
    async def reloadprofile(alias):
        return { "alias": alias }

    @command("server", "track")
    # pylint: disable=E0213
    async def track(alias, idle, status):
        return {
            "alias": alias,
            "idle": int(idle) if idle != "None" else 0, # TODO: `PMFrame.track` "idle" error handling?
            "status": status
        }

    # TODO: deprecate `PMFrame.show_fw` and `PMFrame.DENIED` in favor of the alias to `PMFrame.OK`?
    # @command("server", "show_fw")
    # # pylint: disable=E0211
    # async def show_fw():
    #     pass

    # @command("server", "DENIED")
    # # pylint: disable=E0211
    # async def DENIED():
    #     pass

    @command("server", "time")
    # pylint: disable=E0213
    async def time(timestamp):
        return { "timestamp": float(timestamp) }

    @command("server", "wlonline", aliases=[ "wloffline", "wlapp" ])
    # pylint: disable=E0213
    async def wlonline(alias, since):
        return {
            "name": alias,
            "since": float(since)
        }

    # TODO: deprecate `PMFrame.wloffline` and `PMFrame.wlapp` in favor of the alias to `PMFrame.wlonline`?
    # @command("server", "wloffline")
    # # pylint: disable=E0213
    # async def wloffline(alias, since):
    #     return {
    #         "name": alias,
    #         "since": float(since)
    #     }

    # @command("server", "wlapp")
    # # pylint: disable=E0213
    # async def wlapp(alias, since):
    #     return {
    #         "name": alias,
    #         "since": float(since)
    #     }

    @command("server", "wl")
    # pylint: disable=E0211
    async def friend_list(*friend_list):
        friends = []

        for i in range(0, len(friend_list), 4):
            friend = friend_list[i:i + 4]
            friends.append({
                "alias": friend[0],
                "status_since": float(friend[1]) if friend[1] != None else 0.0,
                "status": friend[2],
                "idle": int(friend[3])
            })

        return { "friend_list": friends }

    @command("server", "wladd")
    # pylint: disable=E0213
    async def add_friend(alias, status, idle):
        return {
            "alias": alias,
            "status": status,
            "idle": int(idle)
        }

    @command("server", "block_list")
    # pylint: disable=E0211
    async def block_list(*the_list):
        return { "list": list(the_list) }

    @command("server", "wldelete")
    # pylint: disable=E0213
    async def remove_friend(alias):
        return { "alias": alias }

    @command("server", "msgoff")
    # pylint: disable=E0213
    async def offline_msg(alias, timestamp, flags, *message):
        return {
            "alias": alias,
            "timestamp": float(timestamp),
            "flags": int(flags), # TODO: parse flags in `PMFrame.msgoff`
            "message": ":".join(message) # TODO
        }

    @command("server", "idleupdate")
    # pylint: disable=E0213
    async def idleupdate(alias, idle):
        return {
            "alias": alias,
            "idle": not bool(int(idle)) # NOTE: `not` to negate
        }

    @command("server", "unblocked")
    # pylint: disable=E0211
    async def unblocked(*aliases):
        # sent when you unblock a person with several blocked aliases
        return { "aliases": list(aliases) }

    @command("server", "status")
    # pylint: disable=E0213
    async def status(alias, idle, status):
      return {
            "alias": alias,
            "idle": int(idle) if idle != "None" else 0, # TODO: `PMFrame.status` "idle" error handling?
            "status": status
        }

    @command("server", "msg")
    # pylint: disable=E0213
    async def message(alias, idk_, timestamp, flags, *message):
        return {
            # TODO: implement `PMFrame.message`
        }

    @command("client", "getblock")
    async def getblock(self):
        pass

    @command("client", "msg")
    async def send_message(self):
        return [
            self["to"],
            self["body"]
        ]

    @command("server", "show_offline_limit")
    # pylint: disable=E0213
    async def show_offline_limit(alias):
        # NOTE: received after 51 (?) messages sent to an offline user
        return { "alias": alias }

    # TODO: deprecate `PMFrame.lowversion` in favor of the alias to `PMFrame.OK`?
    @command("server", "lowversion")
    # pylint: disable=E0211
    async def lowversion():
        pass

    @command("server", "settings")
    # pylint: disable=E0211
    async def settings(*settings):
        pass

    @command("client", "tlogin")
    async def tlogin(self):
        return [
            self["token"],
            self.params.get("version", 2),
            self.params.get("session_id", generate_session_id())
        ]

    @command("client", "wl")
    async def get_friends(self):
        pass

    @command("client", "wladd", aliases=[ "wladd", "wldelete", "track",
                                        "connect", "disconnect" ])
    async def add_friends(self):
        return [ self["alias"] ]

    @command("client", "idle")
    async def send_idle(self):
        return [ 1 if self["idle"] else 0 ]

    @command("client", "block", aliases=[ "unblock" ])
    async def block_user(self):
        return [
            self["alias"],
            self["name"],
            "S" # NOTE: "S" for seller, "A" for anons (disabled)
        ]