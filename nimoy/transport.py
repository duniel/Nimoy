#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

import websockets
import socket

class Transport(object):
    def __init__(self, host, port=8081, ssl=True, terminator="\r\n\0"):
        self.host = host
        self.ssl = ssl
        self.port = port if port else (8081 if self.ssl else 8080)
        use_ssl = "s" if self.ssl else ""
        self.url = f"ws{use_ssl}://{host}:{port}/"
        self.ws = None
        self.write_lock = False
        self.terminator = terminator

    def __repr__(self):
        return (f"<object '{self.__class__.__qualname__}' ({self.url!r}) at "
                f"{hex(id(self))!s}>")

    @property
    def open(self):
        return self.ws.open

    @classmethod
    async def create_connection(cls, *args, **kwargs):
        x = cls(*args, **kwargs)

        await x.connect()

        return x

    async def connect(self):
        use_ssl = "s" if self.ssl else ""
        self.ws = await websockets.connect(self.url,
                                      origin=f"http{use_ssl}://st.chatango.com")

    async def disconnect(self):
        return await self.ws.close()

    async def recv(self):
        data = await self.ws.recv()

        return data.rstrip("\r\n\0")

    async def send_frame(self, frame):
        data = await frame.dump()

        return await self.send_data(*data)

    async def send_data(self, *data):
        data = ":".join([str(x) for x in data])

        if self.write_lock or not self.open:
            return False

        await self.ws.send(f"{data}{self.terminator}")
