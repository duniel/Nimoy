#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

import time
import asyncio
from traceback import format_exc

from .util      import generate_client_id
#from .api       import get_token
from .event     import Event
from .frame     import PMFrame
from .transport import Transport

class PM(object):
    def __init__(self, identity, callback, frame_factory=PMFrame):
        assert callable(callback)

        self.identity = identity
        self.callback = callback

        self.server = "c1.chatango.com"
        self.frame_factory = frame_factory
        self.transport = None
        self.token = getattr(identity, "token", None)

        # unpopulated - font styles
        self.name_color = None
        self.font_size = None
        self.font_color = None
        self.font_face = None

        # unpopulated - chatango metadata
        self.own_name = None
        self.session_id = None
        self.friends = set()

    @classmethod
    async def make(cls, *args, **kwargs):
        g = cls(*args, **kwargs)

        await g.connect()

        return g

    async def connect(self):
        try:
            self.transport = await Transport.create_connection(self.server)

            await self.authenticate()

            asyncio.ensure_future(self.__pinger())

            return await self.__main()
        except Exception as e:
            await self.__emit(Event("error", exception=e, fatal=True,
                                                        traceback=format_exc()))

    async def send_frame(self, frame):
        # NOTE: alias purposes
        await self.transport.send_frame(frame)
        await self.__emit(Event("sent_frame", frame=frame))

    async def send_data(self, *data):
        # NOTE: alias purposes
        await self.transport.send_data(*data)
        await self.__emit(Event("sent_data", data=data))

    async def authenticate(self):
        f = self.frame_factory("tlogin",
            token=self.token
            #username=self.identity.username,
            #password=self.identity.password,
        )

        await self.__emit(Event("sent_auth", frame=f))
        await self.send_frame(f)

    async def __emit(self, event):
        await self.callback(event)

    async def __pinger(self):
        while True:
            await asyncio.sleep(30)
            await self.send_frame(self.frame_factory())
            await self.__emit(Event("sent_ping"))

    async def __main(self):
        while True:
            data = await self.transport.recv()
            await self.__emit(Event("received_data", data=data))

            frame = await self.frame_factory.parse(data)
            await self.__emit(Event("received_frame", frame=frame))

            await self.__process(frame)

    async def __process(self, frame):
        assert isinstance(frame, self.frame_factory)

        if frame.cmd == "":
            await self.__emit(Event("received_ping"))
        elif frame.cmd == "v":
            # TODO?
            pass #await self.__emit(Event())
        elif frame.cmd == "ok":
            pass