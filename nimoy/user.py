#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

from __future__ import annotations

import aiohttp

from .api import (login, is_user, get_msgbg, get_msgstyles, is_premium)
from .error import InvalidLogin

class User(object):
    def __init__(self, username: str, password: str="", token: str="",
                    bg: str="", styles: str="", premium: str="") -> None:
        self.username = username
        self.password = password
        self.bg       = bg
        self.styles   = styles
        self.token    = token
        self.premium  = premium

    def __repr__(self):
        data = ""

        for k, v in self.__dict__.items():
            data += f" {k}={v!r}"

        return f"<{self.__class__.__qualname__}{data}>"

    def __eq__(self, other):
        assert isinstance(other, User)

        return self.username == other.username

    def __ne__(self, other):
        return not self.__eq__(other)

    @classmethod
    async def login(cls, username: str, password: str) -> User:
        if not await is_user(username):
            raise InvalidLogin(f'Username "{username}" is not a Chatango user.')

        token = await login(username, password)

        return cls(username, password, token)

    @classmethod
    async def populate(cls, username: str, password: str="",
                       token: str="") -> User:
        if not await is_user(username):
            raise InvalidLogin(f'Username "{username}" is not a Chatango user.')

        if password and not token:
            token = await login(username, password)

            if not token:
                raise InvalidLogin(f"The supplied login for {username} is not "
                                    "correct")

        bg      = await get_msgbg(username)
        styles  = await get_msgstyles(username)
        premium = await is_premium(username)

        return cls(username, password, token, bg, styles, premium)
