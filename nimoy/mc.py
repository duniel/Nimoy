#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

import asyncio
import time

from .event     import Event
from .frame     import MCFrame
from .transport import Transport

class MessageCatcher(object):
    def __init__(self, identity, callback, server="i0.chatango.com",
                 frame_factory=MCFrame):
        assert callable(callback)

        self.identity = identity
        self.callback = callback

        self.server = server
        self.frame_factory = frame_factory
        self.transport = None
        self.ok_count = 0
        self.message_count = None
        self.messages = set()

    @classmethod
    async def make(cls, *args, **kwargs):
        g = cls(*args, **kwargs)

        await g.connect()

        return g

    async def connect(self):
        try:
            self.transport = await Transport.create_connection(self.server,
                                                              terminator="\r\n")

            await self.send_frame(self.frame_factory("version"))

            asyncio.ensure_future(self.__pinger())
            
            return await self.__main()
        except Exception as e:
            await self.__emit(Event("fatal_error", exception=e, fatal=True))

    async def send_frame(self, frame):
        # NOTE: alias purposes
        await self.transport.send_frame(frame)
        await self.__emit(Event("sent_frame", frame=frame))

    async def send_data(self, *data):
        # NOTE: alias purposes
        await self.transport.send_data(*data)
        await self.__emit(Event("sent_data", data=data))

    async def authenticate(self):
        await self.__emit(Event("sent_auth"))
        await self.send_frame(self.frame_factory("login",
                                            username=self.identity.username,
                                            password=self.identity.password))

    async def __emit(self, event):
        await self.callback(event)

    async def __pinger(self):
        while True:
            await asyncio.sleep(30)
            await self.send_frame(self.frame_factory())
            await self.__emit(Event("sent_ping"))

    async def __main(self):
        while True:
            data = await self.transport.recv()
            await self.__emit(Event("received_data", data=data))

            frame = await self.frame_factory.parse(data)
            await self.__emit(Event("received_frame", frame=frame))

            await self.__process(frame)

    async def __process(self, frame):
        assert isinstance(frame, self.frame_factory)

        if frame.cmd == "":
            await self.__emit(Event("received_ping"))
        elif frame.cmd == "version":
            # TODO?
            pass # await self.__emit(Event())
        elif frame.cmd == "OK":
            # NOTE: implemention detail: server sends OK twice, once after
            #       "version", once after auth
            if self.ok_count == 0:
                await self.authenticate()
            elif self.ok_count == 2:
                # TODO?
                pass # await self.__emit(Event())
            self.ok_count = self.ok_count + 1
        elif frame.cmd == "msgcount":
            self.message_count = frame["num"]
        elif frame.cmd == "msg":
            self.messages.add(frame.params)