#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

import re
import json
import aiohttp
import base64

from typing import (Dict, Optional, Union)

from xml.etree    import ElementTree
from urllib.parse import unquote as url_unquote

from .flag import parse_premium
from .util import get_user_path

_HELLO_ALEC = {
    "origin": "http://st.chatango.com",
    "user-agent": "penis Mozilla/69 vagina Chrome/69 hi alec"
}

# st.chatango.com:  STatic
# fp.chatango.com:  Full Picture?
# m1.chatango.com:  Media
# ust.chatango.com: User STatic
# scripts.st.chatango.com: STatic scripts


async def set_msg_styles(username: str, password: str, **settings):
    username = username.lower()

    url = "https://chatango.com/updatemsgstyles"
    data = {
        "lo":            username,
        "p":             password,
        "hasrec":        1493713126063, # TODO: change? remove? generate? it's the recording timestamp
        "fontFamily":    0,
        "fontSize":      9,
        "textColor":     "ffffff",
        "nameColor":     "4e4e4e",
        "bold":          False,
        "italics":       False,
        "underline":     False,
        "stylesOn":      True,
        "usebackground": 1
    }
    data.update(settings) # defaults

    async with aiohttp.ClientSession() as session:
        async with session.get(url, params=data, headers=_HELLO_ALEC) as res:
            return res.status == 200 and await res.text() == "OK"

async def set_msgbg(username: str, password: str, **settings) -> bool:
    username = username.lower()

    url = "https://chatango.com/updatemsgbg"
    data = {
        "lo":     username,
        "p":      password,
        "hasrec": 0,
        "tile":   0,
        "useimg": 0,
        "ialp":   100,
        "bgc":    "000000",
        "bgalp":  100,
        "align":  "tr",
        "isvid":  0
    }
    data.update(settings) # defaults
    async with aiohttp.ClientSession() as session:
        async with session.get(url, params=data, headers=_HELLO_ALEC) as res:
            return res.status == 200 and await res.text() == "OK"

async def get_ust_file(name: str, file: str,
                       namespace: str="profileimg") -> Union[str, bool]:
    # TODO: refactor `get_ust_file`
    name = name.lower()

    path = get_user_path(name)
    url = f"https://ust.chatango.com/{namespace}/{path}/{file}"

    async with aiohttp.ClientSession() as session:
        async with session.get(url, headers=_HELLO_ALEC) as res:
            if res.status not in [200, 304]:
                return False

            data = await res.read()
            if len(data) == 640 and (data.startswith(b"\xFF\xD8") and
                                     data.endswith(b"\xFF\xD9")):
                # it's a "?" JPEG
                return False

            try:
                return data.decode("utf-8")
            except UnicodeDecodeError:
                return data

async def get_msgstyles(name: str) -> Union[Dict, bool]:
    name = name.lower()

    data = await get_ust_file(name, "msgstyles.json")

    if not data:
        return False

    return json.loads(data)

async def get_msgbg(name: str) -> Union[Dict, bool]:
    name = name.lower()

    data = await get_ust_file(name, "msgbg.xml")

    if not data:
        return False

    tree = ElementTree.fromstring(data)
    return tree.attrib

async def is_group(name: str) -> bool:
    name = name.lower()

    if not re.search(r"^[a-z0-9-]+$", name, re.I) or len(name) > 20:
        return False

    url = f"https://{name}.chatango.com"

    async with aiohttp.ClientSession() as session:
        async with session.get(url, headers=_HELLO_ALEC) as res:
            txt = await res.text()

            return res.status == 200 and 'class="group_desc"' in txt

async def is_user(name: str) -> bool:
    name = name.lower()

    if not re.search(r"^\w+$", name, re.I) or len(name) > 20:
        return False

    url = "https://chatango.com/checkname"
    data = { "name": name }

    async with aiohttp.ClientSession() as session:
        async with session.get(url, params=data, headers=_HELLO_ALEC) as res:
            r = re.search(r"^answer=(.*)&name=\w+$", await res.text())

            if not res.status == 200 or not r:
                return False

            # HACK: refactor this?
            if int(r.group(1)) == 1:
                if await is_group(name):
                    return False
                return True
            return False

async def is_premium(user: str) -> Union[str, bool, None]:
    user = user.lower()

    url = "https://chatango.com/isprem"
    data = { "sid": user }

    async with aiohttp.ClientSession() as session:
        async with session.get(url, params=data, headers=_HELLO_ALEC) as res:
            r = re.search(r"^p=(\d+)$", await res.text(), re.I)

            if not res.status == 200 or not r:
                return False

            return parse_premium(int(r.group(1)))

async def get_msg_bg_img(user: str) -> Optional[bytes]:
    return await get_ust_file(user, "full.jpg")

async def get_full_picture(user: str) -> Optional[bytes]:
    return await get_ust_file(user, "full.jpg")

async def get_thumb_picture(user: str) -> Optional[bytes]:
    return await get_ust_file(user, "thumb.jpg")

async def get_no_image() -> Union[bytes, bool]:
    url = f"https://ust.chatango.com/profileimg/noimage.jpg"

    async with aiohttp.ClientSession() as session:
        async with session.get(url, headers=_HELLO_ALEC) as res:
            if res.status not in [200, 304]:
                return False

            return res.read()

async def get_group_profile(name: str) -> Optional[Dict]:
    # TODO: refactor this
    file_ = await get_ust_file(name, "gprofile.xml", "groupinfo")

    if file_:
        tree  = ElementTree.fromstring(file_)
        ret   = {
            "title":       None,
            "description": None
        }

        for c in tree:
            if c.tag == "title":
                ret["title"]       = url_unquote(c.text)
            elif c.tag == "desc":
                ret["description"] = url_unquote(c.text)

        return ret

async def login(username: str, password: str) -> Optional[str]:
    username = username.lower()

    url = "https://chatango.com/login"
    data = {
        "user_id": username,
        "password": password,
        "storecookie": "on",
        "checkerrors": "yes"
    }

    async with aiohttp.ClientSession() as session:
        async with session.get(url, headers=_HELLO_ALEC, params=data) as res:
            cookie = str(res.cookies["auth.chatango.com"])

        match = re.search(r"auth\.chatango\.com=(?P<token>\w+)", cookie)
        if match:
            token = match.group("token")
            if token:
                return token

async def signout(token):
    url = "https://chatango.com/signout"
    data = {
        "noredirect": True,
        "token": token
    }

    async with aiohttp.ClientSession() as session:
        async with session.get(url, params=data, headers=_HELLO_ALEC) as res:
            return res.status == 200

async def get_current_revision():
    url = "https://st.chatango.com/cfg/nc/r.json"

    async with aiohttp.ClientSession() as session:
        async with session.get(url, headers=_HELLO_ALEC) as res:
            if res.status != 200:
                return False

            data = json.loads(await res.text())

            return data["r"]

async def get_mini_profile(user: str) -> Dict:
    pass # mod1.xml

async def get_full_profile(user: str) -> Dict:
    pass # mod2.xml

async def get_own_full_profile(username, password, settings):
    pass # POST /updateprofile u=&p=&auth=pwd&arch=h5&src=group

async def set_cookies(user, password):
    pass # POST /setcookies pwd=&sid=

async def signuptag(user: str):
    pass # POST /signuptag email=&login=&password=&password_confirm=&checkerrors=yes[&group_url=&group_title=]

async def update_group(group, foobar):
    pass # POST /updategroupprofile erase=&l=&d=&n=&u=[&ptoken=&lo=&p=]