#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

#import asyncio
from types import MethodType
from functools import partial

from .user import User
from .group import Group
from .pm import PM
from .mc import MessageCatcher

class Client(object):
    def __init__(self, username=None, password=None, use_pm=False):
        self._username = username
        self._password = password
        self.use_pm    = bool(use_pm)

        self.identity = None
        self.groups   = dict()
        self.pm       = None

    async def leave_all(self):
        for g in self.groups.values():
            if g.connected:
                await g.leave()

        return all(not g.connected for g in self.groups.values())

    async def join_group(self, name):
        assert isinstance(name, str)

        name = name.lower()

        group = await Group.join(name, self.identity, self.handle_event)

        self.groups[name] = group

        return group

    async def leave_group(self, name):
        assert isinstance(name, str)

        name = name.lower()

        if name in self.groups:
            g = await self.get_group(name)

            g.leave()

            return True

        return False

    async def get_group(self, name):
        assert isinstance(name, str)

        name = name.lower()

        return self.groups.get(name)

    async def stop(self):
        #await self.pm.leave()

        for group in self.groups.values():
            await group.leave()

    async def handle_event(self, group, event):
        # NOTE: should be shadowed by subclasses!
        pass
