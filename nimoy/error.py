#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

class MissingFrame(Exception):
    # NOTE: Frame does not exist
    pass

class WrongVersion(Exception):
    # NOTE: Wrong protocol version
    pass

class InvalidFlag(Exception):
    # NOTE: Used in `nimoy.flag`, self explanatory
    pass

class InvalidLogin(Exception):
    # NOTE: Used when supplied logins are invalid
    pass