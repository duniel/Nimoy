#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

# TODO: refactor this, or replace it with a namedtuple or similar

class Event(object):
    def __init__(self, kind, **kw):
        self.kind  = kind
        self._data = kw

    def __getattr__(self, identifier):
        return self._data[identifier]

    def __repr__(self):
        data = ""

        if self._data:
            data = ":"

        for k, v in self._data.items():
            if k is not "kind":
                data += f" {k}={v!r}"

        return f"<{self.__class__.__name__} {self.kind!r}{data}>"
