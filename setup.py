#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; tab-width: 4 -*-
# vim: ft=python fenc=utf-8

from setuptools import setup
import os.path

def read_file(name):
    path = os.path.join(os.path.dirname(__file__), name)

    with open(path, "r") as f:
        return f.read()

setup(
    name="Nimoy",
    version="0.1",
    description="A modern Chatango library created in memory of Leonard Nimoy",
    long_description=read_file("README.md"),
    keywords="modern async chatango library nimoy".split(),
    url="https://nimoy.no/",
    author="Daniel Isaksen",
    author_email="d@duniel.no",
    license="MIT",
    packages=[ "nimoy" ],
    zip_safe=True,
    install_requires=read_file("requirements.txt").splitlines(),
    python_requires=">=3.6",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Intended Audience :: Developers",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Development Status :: 3 - Alpha",
        "Operating System :: POSIX",
        "Operating System :: MacOS :: MacOS X",     # ?
        "Operating System :: Microsoft :: Windows", # ?
        "Framework :: AsyncIO",
        "Topic :: Software Development",
        "Topic :: Software Development :: Libraries",
        "Topic :: Communications :: Chat"
    ]
)