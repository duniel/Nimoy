# Nimoy

A modern Chatango library created in memory of Leonard Nimoy. Inspired by Lumirayz' `chcl_thing`, `WIC`, and ever-so-slightly by his `ch.py`.

* Nimoy's website is [https://nimoy.no/](https://nimoy.no/).

* Botteh's website is [https://botteh.no/](https://botteh.no/).

## Preliminary

### What Nimoy is

Nimoy is a versatile and feature-compliant Chatango library written with the latest Python 3.6 cake-eating technology. It can be used in various software connecting to Chatango, such as chat bots and other clients.

### Why Nimoy

I decided to revive [Botteh](https://gitlab.com/duniel/Botteh), which needed a proper library to work optimally. This lead to the creation of `lib`, later renamed to Nimoy. In spirit of the Vulcan ways (as well as logic), this library was of course open sourced under the MIT license despite the original concerns regarding `ch.py`.

### Developers

* [Dynamic](https://dynamic.chatango.com/): Lead developer.
* [Sorch](https://sorch.chatango.com/): Programmer.

### A simple walkthrough (alphabetical)

To interact with HTTP services provided by Chatango, look no further than [`nimoy.api`](nimoy/api.py).

If you just want a bot, you can subclass `nimoy.client.Client` in [`nimoy.client`](nimoy/client.py).

The following bullet points are in alphabetical order.

* Exceptions raised by Nimoy can be found in [`nimoy.errors`](nimoy/errors.py).

* Group events are instances of the class `Event` found in [`nimoy.event`](nimoy/event.py) - **but beware**, it might be removed in favor of a `namedtuple` or something similar!

* Chatango uses [bit packing](https://wiki.python.org/moin/BitManipulation) and various other integers to transmit statuses, flags, and error messages. They can be found in [`nimoy.flag`](nimoy/flag.py).

* Both client and server tagserver commands reside in [`nimoy.frame`](nimoy/frame.py). If you want to implement or change how frames are parsed and created, this is your go-to file.

* To interface directly with a group (`nimoy.client` does this for you), you can use the `Group` class from [`nimoy.group`](nimoy/group.py).

* The same goes for Chatango Message Catcher, in [`nimoy.mc`](nimoy/mc.py).

* [`nimoy.message`](nimoy/message.py) exists for all your message parsing and creating needs!

* As with groups, you can interface with Chatango PMs (private messages) through the `PM` class in [`nimoy.pm`](nimoy/pm.py).

* Calculating the Chatango tagserver number (`s#.chatango.com`) is done in [`nimoy.tagserver`].

* The actual websocket is created and handled by [`nimoy.transport`](nimoy/transport.py).

* Identities/users are in [`nimoy.user`](nimoy/user.py), this is used as an abstraction around users, including the user used for authentication.

* For miscellaneous utility functions, you have [`nimoy.util`](nimoy/util.py).

## Status

### Coverage

TODO

### Major TODOs

* [X] Implement `nimoy.group`.
* [ ] Implement `nimoy.pm`.
* [ ] Implement `nimoy.mc`.
* [ ] Implement `nimoy.user` properly.
* [ ] Rename `nimoy.mc` to `nimoy.messagecatcher`?
* [ ] Handle all frames in `nimoy.group`.
* [ ] Handle all frames in `nimoy.pm`.
* [ ] Handle all frames in `nimoy.pm`.
* [ ] Create a nice API for `nimoy.message` - styles, colors, newlines, etc.
* [ ] Implement private messages (`PMMessage`?) in `nimoy.message`.
* [ ] Finish `nimoy.api`.
* [ ] Support regular sockets in `nimoy.transport`.
* [ ] Unit and integration testing, especially for `nimoy.frame`, `nimoy.group`, `nimoy.pm`, and `nimoy.mc`.
* [ ] Write inline documentations with docstrings (`__doc__` / `"""`-strings)
* [X] Async-ify everything.
* [ ] Make everything [Mypy](http://mypy-lang.org/)-compatible. Type annotations everywhere!
* [X] Make it into a proper `setuptools` package with `setup.py` and everything!

## Install

### Dependencies

See [`requirements.txt`](requirements.txt). Install them with `pip3 install --user -r requirements.txt`.

### Installation

Soon enough, you should be able to run `pip3 install nimoy`. This is work in progress.

For now, you can clone this Git repository and run `python3 setup.py install` or clone it and extract the folder `nimoy` into your bot directory, as follows:

```sh
/home/you $ git clone https://gitlab.com/duniel/nimoy.git
/home/you $ mv nimoy/nimoy your-bot/
/home/you $ rm -r nimoy
```

The actual Python module is in the directory [`nimoy`](nimoy/).

### Examples

TODO

### API documentation

TODO

### License

Licensed under the MIT license. See the `LICENSE` file.